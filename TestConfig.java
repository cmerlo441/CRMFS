package edu.ncc.cmerlo.crmfs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class TestConfig {
    public static void main( String args[] ) {
        Config c1 = new Config();
        System.out.println( c1 );

        Config c2 = new Config( "user", "host", "dir", "mp" );
        System.out.println( c2 );

        @SuppressWarnings( "unused" )
        Config c3 = null;

        try {
            c3 = new Config( "this should not work", "host", "dir", "mp" );
        } catch( ConfigException ex ) {
            System.out.println( ex );
        }

        Config c4 = new Config( " user ", "host", "dir", "mp" );
        System.out.println( c4 );

        Config c5 = new Config( "user", "host", "mp", "dir", "vol" );
        System.out.println( c5 );

        ArrayList<Config> configs = null;
        try {
            configs = Config.readFile( new File( "/Users/cmerlo/.sshfs" ) );
        } catch( FileNotFoundException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        for( Config c : configs ) {
            System.out.println( c );
        }
    }

}
