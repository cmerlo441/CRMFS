package edu.ncc.cmerlo.crmfs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Display information about CRMFS configurations, and provide mounting,
 * unmounting, and editing tools
 * 
 * @author cmerlo
 *
 */
public class CRMFSPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    ArrayList<Config>         configs          = null;

    private JPanel            details;

    public CRMFSPanel() {

        // Read configs
        try {
            String homedir = System.getProperty( "user.home" );
            configs = Config.readFile( new File( homedir + "/.crmfs" ) );
        } catch( FileNotFoundException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        details = new JPanel();
        details.setLayout( new GridLayout( configs.size() + 1, 2 ) );
        details.add( new JLabel( "Actions" ), JLabel.CENTER );
        details.add( new JLabel( "Remote host : Remote directory" ),
                JLabel.CENTER );
        for( Config c : configs ) {
            details.add( new JLabel(
                    c.getHostname() + ":" + c.getRemoteDirectory() ) );

            JPanel buttonsPanel = new JPanel( new GridLayout( 1, 2 ) );
            JButton detailsButton = new JButton( "Details" );
            JButton mountUnmountButton = null;
            try {
                mountUnmountButton = new JButton(
                        c.isMounted() ? "Unmount" : "Mount" );
            } catch( IOException ex ) {
                System.err.println( ex );
            }
            buttonsPanel.add( detailsButton );
            buttonsPanel.add( mountUnmountButton );

            detailsButton.addActionListener( new DetailsListener( c, this ) );
            mountUnmountButton.addActionListener(
                    new MountUnmountListener( mountUnmountButton, c ) );

            details.add( buttonsPanel );
        }
        this.add( details, BorderLayout.WEST );

    }

    public static void main( String[] args ) {
        // TODO Auto-generated method stub
        JFrame jframe = new JFrame( "CRMFS" );
        jframe.setLayout( new BorderLayout() );
        jframe.add( new CRMFSPanel() );

        JLabel titleLabel = new JLabel( "CRMFS Version 0.1" );
        titleLabel.setHorizontalAlignment( JLabel.CENTER );
        jframe.add( titleLabel, BorderLayout.NORTH );

        jframe.setMinimumSize( new Dimension( 500, 300 ) );
        jframe.pack();
        jframe.setVisible( true );
        jframe.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }

}
