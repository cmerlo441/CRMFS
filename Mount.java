package edu.ncc.cmerlo.crmfs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Provide command-line access to CRMFS functions
 * 
 * @author cmerlo
 *
 */
public class Mount {
    public static void main( String args[] ) {

        // String exName = new java.io.File( Mount.class.getProtectionDomain()
        // .getCodeSource().getLocation().getPath() ).getName();
        // System.err.println( exName );

        ArrayList<Config> configs = null;
        ArrayList<String> mounts = new ArrayList<>();
        boolean anyMounts = false;

        for( String s : args )
            mounts.add( s );

        try {
            String homedir = System.getProperty( "user.home" );
            configs = Config.readFile( new File( homedir + "/.crmfs" ) );
        } catch( FileNotFoundException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for( Config c : configs ) {
            try {
                if( mounts.isEmpty() || mounts.contains( c.getVolumeName() )
                        || mounts.contains( c.getMountPoint() ) ) {
                    c.mount();
                    anyMounts = true;
                }
            } catch( IOException e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch( MountedException ex ) {
                System.err.println( ex );
            }

        }
        if( !anyMounts ) {
            System.err.println( "No systems mounted." );
            System.exit( 1 );
        }
        System.exit( 0 );
    }
}
