package edu.ncc.cmerlo.crmfs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * <p>
 * Title: Config class
 * </p>
 * 
 * <p>
 * Description: Represent one configuration of CRMFS
 * </p>
 * 
 * @author Christopher R. Merlo <cmerlo@ncc.edu>
 *
 */
public class Config {
    private String username;
    private String hostname;
    private String remoteDirectory;
    private String mountPoint;
    private String volumeName;

    /**
     * Initializes a config with dummy values
     */
    public Config() {
        username = "none";
        hostname = "none";
        remoteDirectory = "none";
        mountPoint = "none";
        volumeName = null;
    }

    /**
     * Initializes a config with username, hostname, and mount point
     * 
     * @param u
     *            Remote server's username
     * @param h
     *            Remote server's hostname (or IP address)
     * @param m
     *            Mount point on local host
     */

    public Config( String u, String h, String r, String m ) {
        if( !isFormatOK( u ) )
            throw new ConfigException( "username \"" + u.trim() + "\"" );
        if( !isFormatOK( h ) )
            throw new ConfigException( "hostname \"" + h.trim() + "\"" );
        if( !isFormatOK( r ) )
            throw new ConfigException( "remote dir \"" + r.trim() + "\"" );
        if( !isFormatOK( m ) )
            throw new ConfigException( "mount point \"" + m.trim() + "\"" );
        username = u.trim();
        hostname = h.trim();
        remoteDirectory = r.trim();
        mountPoint = m.trim();
    }

    /**
     * Initializes a config with username, hostname, mount point, and volume
     * name
     * 
     * @param u
     *            Remote server's username
     * @param h
     *            Remote server's hostname (or IP address)
     * @param m
     *            Mount point on local host
     * @param v
     *            What to call the remote host locally
     */
    public Config( String u, String h, String r, String m, String v ) {
        this( u, h, r, m );
        if( !isFormatOK( v ) )
            throw new ConfigException( "volume name \"" + v.trim() + "\"" );
        volumeName = v.trim();
    }

    private boolean isFormatOK( String s ) {
        return s.trim().matches( "^[^ \t\n]+$" );
    }

    public String getUsername() {
        return username;
    }

    public String getHostname() {
        return hostname;
    }

    public String getRemoteDirectory() {
        return remoteDirectory;
    }

    public String getMountPoint() {
        return mountPoint;
    }

    public boolean hasVolumeName() {
        return volumeName != null;
    }

    public String getVolumeName() {
        return volumeName;
    }

    public boolean isMounted() throws IOException {
        Process p = Runtime.getRuntime().exec( "mount" );
        BufferedReader mountOutput = new BufferedReader(
                new InputStreamReader( p.getInputStream() ) );
        String line;
        while( ( line = mountOutput.readLine() ) != null ) {
            if( line.contains( mountPoint ) )
                return true;
        }
        return false;
    }

    public void mount() throws MountedException, IOException {
        if( isMounted() )
            throw new MountedException( mountPoint );
        String executable = "/usr/local/bin/sshfs";
        String mountCommand = null;
        if( hasVolumeName() )
            mountCommand = String.format(
                    executable + " -ovolname=%s %s@%s:%s %s", getVolumeName(),
                    getUsername(), getHostname(), getRemoteDirectory(),
                    getMountPoint() );
        else
            mountCommand = String.format( executable + " %s@%s:%s %s",
                    getUsername(), getHostname(), getRemoteDirectory(),
                    getMountPoint() );
        try {
            new File( mountPoint ).mkdirs();
            Runtime.getRuntime().exec( mountCommand );
        } catch( IOException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void unmount() throws NotMountedException, IOException {
        if( !isMounted() )
            throw new NotMountedException( mountPoint );
        Runtime.getRuntime().exec( "umount " + mountPoint );
    }

    @Override
    public boolean equals( Object o ) {
        Config c = ( Config )o;
        return username.equals( c.username ) && hostname.equals( c.hostname );
    }

    @Override
    public String toString() {
        StringBuffer returnMe = new StringBuffer( "User: " );
        returnMe.append( username );
        returnMe.append( "\n" );

        returnMe.append( "Host: " );
        returnMe.append( hostname );
        returnMe.append( "\n" );

        returnMe.append( "Remote directory: " );
        returnMe.append( remoteDirectory );
        returnMe.append( "\n" );

        returnMe.append( "Mount point: " );
        returnMe.append( mountPoint );
        returnMe.append( "\n" );

        if( volumeName != null ) {
            returnMe.append( "Volume name: " );
            returnMe.append( volumeName );
            returnMe.append( "\n" );
        }

        return returnMe.toString();
    }

    /**
     * Read many configurations from a file
     * 
     * @param f
     *            File object to read from
     * @return An ArrayList of configurations
     * @throws FileNotFoundException
     *             if the file ain't there
     */
    public static ArrayList<Config> readFile( File f )
            throws FileNotFoundException {
        ArrayList<Config> configs = new ArrayList<>();
        Scanner s;

        try {
            s = new Scanner( f );
        } catch( FileNotFoundException ex ) {
            throw ex;
        }
        while( s.hasNextLine() ) {
            String line = s.nextLine();
            // System.out.println( "Line: " + line );
            if( line.trim().length() > 0 && line.charAt( 0 ) != '#' ) {
                StringTokenizer st = new StringTokenizer( line );
                String u = st.nextToken();
                String h = st.nextToken();
                String r = st.nextToken();
                String m = st.nextToken();

                // volumeName is optional
                if( st.hasMoreTokens() ) {
                    configs.add( new Config( u, h, r, m, st.nextToken() ) );
                } else {
                    configs.add( new Config( u, h, r, m ) );
                }
            }
        }
        s.close();
        return configs;
    }
}
