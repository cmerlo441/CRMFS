package edu.ncc.cmerlo.crmfs;

/**
 * <p>
 * Title: ConfigException Class
 * </p>
 *
 * <p>
 * Description: Thrown when a bad config option is set
 * </p>
 */
public class ConfigException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Initializes a ConfigException
     * 
     * @param m
     *            Details about the bad config option
     */
    public ConfigException( String m ) {
        super( "Bad configuration option: " + m );
    }
}
