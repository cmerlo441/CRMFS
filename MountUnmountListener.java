package edu.ncc.cmerlo.crmfs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

/**
 * Handles pushing a mount or unmount button in CRMFSPanel
 * 
 * @author cmerlo
 *
 */
public class MountUnmountListener implements ActionListener {

    Config  config;
    JButton button;

    public MountUnmountListener( JButton button, Config config ) {
        this.button = button;
        this.config = config;
    }

    @Override
    public void actionPerformed( ActionEvent e ) {
        System.err.println( config );
        try {
            if( config.isMounted() ) {
                config.unmount();
                button.setText( "Mount" );
            } else {
                config.mount();
                button.setText( "Unmount" );
            }
        } catch( NotMountedException | IOException e1 ) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

}
