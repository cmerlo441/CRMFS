package edu.ncc.cmerlo.crmfs;

import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Shows the details of one configuration
 */
public class DetailsPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Config            config;
    private JPanel            guts;
    private boolean           isMounted;

    private JLabel            labels[]         = { new JLabel( "Remote Host" ),
            new JLabel( "Remote directory" ), new JLabel( "Remote username" ),
            new JLabel( "Local mount point" ),
            new JLabel( "Local volume name" ) };
    private JTextField        textfields[];

    public DetailsPanel( Config c ) {
        config = c;
        try {
            isMounted = config.isMounted();
        } catch( IOException e ) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        guts = new JPanel( new GridLayout( 5, 2 ) );

        textfields = new JTextField[ 5 ];
        textfields[ 0 ] = new JTextField( config.getHostname() );
        textfields[ 1 ] = new JTextField( config.getRemoteDirectory() );
        textfields[ 2 ] = new JTextField( config.getUsername() );
        textfields[ 3 ] = new JTextField( config.getMountPoint() );
        textfields[ 4 ] = new JTextField( config.getVolumeName() );

        for( int i = 0; i < 5; ++i ) {
            guts.add( labels[ i ] );
            guts.add( textfields[ i ] );
            textfields[ i ].setEditable( !isMounted );
        }

        add( guts );
    }
}
