package edu.ncc.cmerlo.crmfs;

/**
 * <p>
 * Title: ConfigException Class
 * </p>
 *
 * <p>
 * Description: Thrown when trying to mount a mounted drive
 * </p>
 */
public class MountedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Initializes a ConfigException
     * 
     * @param m
     *            Details about the mounted system
     */
    public MountedException( String m ) {
        super( m + " is already mounted" );
    }
}
