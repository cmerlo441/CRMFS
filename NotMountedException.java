package edu.ncc.cmerlo.crmfs;

/**
 * <p>
 * Title: ConfigException Class
 * </p>
 *
 * <p>
 * Description: Thrown when trying to unmount a drive that's not mounted
 * </p>
 */
public class NotMountedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Initializes a ConfigException
     * 
     * @param m
     *            Details about the non-mounted system
     */
    public NotMountedException( String m ) {
        super( m + " is not mounted" );
    }
}
