package edu.ncc.cmerlo.crmfs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * Handle pressing the "Details" button in CRMFSPanel
 * 
 * @author cmerlo
 *
 */
public class DetailsListener implements ActionListener {

    private Config     config;
    private CRMFSPanel panel;

    public DetailsListener( Config c, CRMFSPanel p ) {
        config = c;
        panel = p;
    }

    @Override
    public void actionPerformed( ActionEvent e ) {
        JOptionPane.showMessageDialog(
                panel, new DetailsPanel( config ), "Details for "
                        + config.getHostname() + ":" + config.getMountPoint(),
                0 );

    }

}
